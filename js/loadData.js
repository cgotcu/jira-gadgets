var buildLoadDataService = function (jiraRootUrl) {

    if (_.isUndefined(jiraRootUrl)) {
        jiraRootUrl = "";
    }


//TODO Ensure these are the correct custom field names to use.
    var customFields = {
        startTime: "customfield_10004",
        endTime: "customfield_10003",
        link: "self",
        componentVersions: [
            "customfield_10000",
            "customfield_10001",
            "customfield_10002",
            "customfield_10007",
            "customfield_10008",
            "customfield_10009"
        ]
    };


    /**
     * Convert an array of promises into a single promise (of an array)
     * @param arrayOfPromises
     * @returns {*}
     */
    function waitForAll(arrayOfPromises) {
        return $.when.apply($, arrayOfPromises).then(function () {
            return _.map(arguments);
        });
    }

    /**
     *
     * @param timestamp
     * @param midnightIsEnd Whether midnight should be treated as beginning of day (0) or end of day (48)
     */
    function timestampToTimeIndex(timestamp, midnightIsEnd) {
        var hour = timestamp.substring(11, 13);
        var isBottomOfHour = timestamp[14] == 3;

        var index = hour * 2;
        if (isBottomOfHour) {
            index += 1;
        }
        if (midnightIsEnd && index == 0) {
            index = 48;
        }


        return index;
    }


    function deferredSubtask(url) {
        console.log("loading subtask " + url);
        return $.ajax(url).then(function (json) {
            return {
                description: json.fields.summary,
                start: timestampToTimeIndex(json.fields[customFields.startTime], false),
                end: timestampToTimeIndex(json.fields[customFields.endTime], true)
            };
        });
    }

    function deferredIssue(raw) {

        var subtaskPromises = _.map(raw.fields.subtasks, function (s) {
            var url = s.self;
            return deferredSubtask(url);
        });

        return waitForAll(subtaskPromises).then(function (subtasks) {
            return {
                name: raw.fields.summary,
                link: raw.self,
                deployedComponents: _.map(customFields.componentVersions, function (i) {
                    return raw.fields[i] || "";
                }),
                todaysEvents: _.sortBy(subtasks, "start")
            }
        });
    }


    function main(date) {
        if (_.isUndefined(date)) {
            date = "now()";
        }
        var query = jiraRootUrl + "/rest/api/2/search?jql=" + encodeURIComponent('"Valid for" = ' + date) + "&expand=names";

        return $.ajax(query).then(function (json) {
            var issuePromises = _.map(json.issues, deferredIssue);

            return waitForAll(issuePromises)
                .then(function (issues) {
                    return {
                        componentNames: _.map(customFields.componentVersions, function (i) {
                            return json.names[i];
                        }),
                        environments: issues
                    }
                });
        });
    }

    return main;
};


