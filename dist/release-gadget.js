function TableView($table){
    this.$table = $table;
    this.$envTr = $("<tr>");
    this.cursor = 0;
}

TableView.fn = TableView.prototype;

/**
 * Visually represent report data in html table
 * @method renderTable
 * @param data Custom report data from JIRA
 * @returns {*}
 */
TableView.fn.renderTable = function(data){
    this._labelComponentHeaders(data.componentNames);
    this._appendEnvRows(data.environments);
};

/**
 * Label component headers by setting <th> text values for component columns (2 through 6)
 * @method {private} _labelComponentHeaders
 * @param {[]} names Component names
 * @returns {*}
 */
TableView.fn._labelComponentHeaders = function(names){
    _.each(names, function(name, i){
        $(".component." + (i + 1)).text(name).attr("title", name);
    });
};

/**
 * Append environment rows to report table. Each row has an environment's name, component versions, and schedule
 * of events for the day.
 * @method {private} _appendEnvRows
 * @param {[]} environments JIRA environments - dev, test, prod for example
 * @returns {*}
 */
TableView.fn._appendEnvRows = function(environments){

    _.each(environments, function(env){
        this.$envTr = $('<tr>');

        this._appendComponentTds(env.name, env.deployedComponents);
        this._appendEventTds(env.todaysEvents);

        this.$table.append(this.$envTr);
    }, this);

};

// append the environment's name and component version <td>s
TableView.fn._appendComponentTds = function(envName, componentVersions){
    var $envTds =
        $("<td class='name'><td class='1'><td class='2'><td class='3'><td class='4'><td class='5'><td class='6 borderRight'>")
            .addClass("component");
    var unknownVersion = true;

    $envTds.first("td.name").text(envName).attr("title", envName);

    // test the version string for a "?". Add class ? so it will be marked as yellow
    _.each(componentVersions, function(version, i){
        unknownVersion = /\?/.test(version);

        if(unknownVersion){
            $envTds.next("td." + (i + 1)).text(version).attr({"title": version, "class": "unknownVersion"})
        } else {
            $envTds.next("td." + (i + 1)).text(version).attr("title", version);
        }
    });

    this.$envTr.append($envTds);

};

// append the environment's daily event <td>s
TableView.fn._appendEventTds = function(events){
    var start = 0;
    var end = 0;
    var eventSpan = 0;
    var blankSpan = 0;
    var eventClasses = ["event"];
    var titleText = "";

    this.cursor = 0;

    _.each(events, function(event){
        start = event.start;
        end = event.end;
        eventSpan = end - start;
        eventClasses.push(colorEncode(event.description));
        titleText = (start / 2) + "-" + (end / 2) + " " + event.description;

        // if start is past the cursor, then a blank must be appended before appending the event td
        if(start > this.cursor){
            blankSpan = start - this.cursor;
            this.$envTr.append($("<td>").attr("colspan", blankSpan).addClass("blank"));
        }

        this.$envTr.append($("<td>").attr({"colspan": eventSpan, "title": titleText, "class": eventClasses.join(" ")})
            .text(event.description)).click(function(e) {
                window.open(event.link,'Activity')
                e.stopPropagation();
            });

        this.cursor = end;
    }, this);

    // fill in left over time from last event until midnight with blank space
    if(this.cursor < 48){
        blankSpan = 48 - this.cursor;
        this.$envTr.append($("<td>").attr("colspan", blankSpan).addClass("blank"));
    }

    function colorEncode(description){
        return /internal/i.test(description) ? "internal" :
            /external/i.test(description) ? "external" :
            /security/i.test(description) ? "sca" :
            /maintenance|MW/i.test(description) ? "maintenance" : "";

    }
};;

var buildLoadDataService = function (jiraRootUrl) {

    if (_.isUndefined(jiraRootUrl)) {
        jiraRootUrl = "";
    }


//TODO Ensure these are the correct custom field names to use.
    var customFields = {
        startTime: "customfield_10004",
        endTime: "customfield_10003",
        componentVersions: [
            "customfield_10000",
            "customfield_10001",
            "customfield_10002",
            "customfield_10007",
            "customfield_10008",
            "customfield_10009"
        ]
    };


    /**
     * Convert an array of promises into a single promise (of an array)
     * @param arrayOfPromises
     * @returns {*}
     */
    function waitForAll(arrayOfPromises) {
        return $.when.apply($, arrayOfPromises).then(function () {
            return _.map(arguments);
        });
    }

    /**
     *
     * @param timestamp
     * @param midnightIsEnd Whether midnight should be treated as beginning of day (0) or end of day (48)
     */
    function timestampToTimeIndex(timestamp, midnightIsEnd) {
        var year = timestamp.substring(0, 4);
        var month = timestamp.substring(5, 7);
        var day = timestamp.substring(8, 10);
        var hour1 = timestamp.substring(11, 13);
        var minute = timestamp.substring(14, 16);

        var utcdate = new  Date(year, month, day, hour1, minute);
        var dcdate = new Date(utcdate.getTime() + (-5 * 3600000));

        alert("Went from day " + utcdate.getDay() + " at hour" + utcdate.getHours() + ":" + utcdate.getMinutes() + " to day "
            + dcdate.getDay() + " at " + dcdate.getHours() + ":" + dcdate.getMinutes());

        //var hour = timestamp.substring(11, 13);
        var hour = dcdate.getHours();
        //var isBottomOfHour = timestamp[14] == 3;
        var isBottomOfHour = (dcdate.getHours().toString().substr(0, 1) == "3");

        var index = hour * 2;
        if (isBottomOfHour) {
            index += 1;
        }
        if (midnightIsEnd && index == 0) {
            index = 48;
        }

        if (midnightIsEnd && dcdate.getMinutes() == 59) {
            index = 48;
        }

        return index;
    }


    function deferredSubtask(url) {
        console.log("loading subtask " + url);
        return $.ajax(url).then(function (json) {
            return {
                description: json.fields.summary,
                start: timestampToTimeIndex(json.fields[customFields.startTime], false),
                end: timestampToTimeIndex(json.fields[customFields.endTime], true)
            };
        });
    }

    function deferredIssue(raw) {

        var subtaskPromises = _.map(raw.fields.subtasks, function (s) {
            var url = s.self;
            return deferredSubtask(url);
        });

        return waitForAll(subtaskPromises).then(function (subtasks) {
            return {
                name: raw.fields.summary,
                deployedComponents: _.map(customFields.componentVersions, function (i) {
                    return raw.fields[i] || "";
                }),
                todaysEvents: _.sortBy(subtasks, "start")
            }
        });
    }


    function main(date) {
        if (_.isUndefined(date)) {
            date = "now()";
        }
        var query = jiraRootUrl + "/rest/api/2/search?jql=" + encodeURIComponent('"Valid for" = ' + date) + "&expand=names";

        return $.ajax(query).then(function (json) {
            var issuePromises = _.map(json.issues, deferredIssue);

            return waitForAll(issuePromises)
                .then(function (issues) {
                    return {
                        componentNames: _.map(customFields.componentVersions, function (i) {
                            return json.names[i];
                        }),
                        environments: issues
                    }
                });
        });
    }

    return main;
};


