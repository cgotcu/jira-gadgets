module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                // define a string to put between each file in the concatenated output
                separator: ';\n\n'
            },
            dist: {
                // the files to concatenate
                src: ['js/*.js'],
                // the location of the resulting JS file
                dest: 'dist/<%= pkg.name %>.js'
            }
        },
        uglify: {
            options: {
                mangle: true,
                // the banner is inserted at the top of the output
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                },
                report: "gzip"
            }
        },
        qunit: {
            files: ["js/test/tableViewTest.html"]
        },
        "string-replace": {
            inline: {
                files: {
                    'dist/release-gadget.xml': "gadget-template.xml"
                },
                options: {
                    replacements: [{
                        pattern: '{{insert javascript here}}',
                        replacement: '<script><%= grunt.file.read("dist/release-gadget.min.js") %></script>'
                    },
                    {
                        pattern: '{{insert styles here}}',
                        replacement: '<style><%= grunt.file.read("css/style.css") %></style>'
                    },
                    {
                        pattern: '{{insert jquery here}}',
                        replacement: '<script><%= grunt.file.read("js/lib/jquery.min.js") %></script>'

                    }
//                    {
//                        pattern: '{{insert lodash here}}',
//                        replacement: '<style><%= grunt.file.read("js/lib/jquery.min.js") %></style>'
//                    }
                    ]
                }
            }
        },
        clean: ["dist"]
    });

    // Load the plugins
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-contrib-qunit');

    grunt.registerTask('default', ['clean', 'concat', 'uglify', 'string-replace']);
};